#!/usr/bin/env python3

import setuptools

setuptools.setup(
    name='python-template',
    version='0.1',
    description='My python project template',
    author='Liam Quinn',
    packages=setuptools.find_packages()
)
